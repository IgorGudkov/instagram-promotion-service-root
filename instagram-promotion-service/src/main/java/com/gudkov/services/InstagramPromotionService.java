package com.gudkov.services;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author Igor Gudkov.
 */

@SpringBootApplication
public class InstagramPromotionService {

    public static void main(String[] args) {
        SpringApplication.run(InstagramPromotionService.class, args);
    }
}